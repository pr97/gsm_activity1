package com.jain.gsm_activity_Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

import com.jain.gsm_activity_Bean.Gsm_Info_Bean;
import com.jain.gsm_activity_Bean.Gsm_System_Bean_Store;
import com.jain.gsm_activity_Bean.Gsm_System_Bean_Warehouse;
import com.jain.gsm_activity_Bean.Storage;
import com.jain.gsm_activity_Bean.Total_Helper;

public class Gsm_System_Service implements Service_reader {
	
	public Gsm_System_Service() {
		// TODO Auto-generated constructor stub
		init();
		
		
	}
	public boolean init()
	{
		System.out.println("\t "+Gsm_Info_Bean._Storename+"\t Owened by: "+Gsm_Info_Bean._owner);
		System.out.println(" Address: "+Gsm_Info_Bean._Stroreaddress+"\t Phone: "+Gsm_Info_Bean._storeMobile);
		System.out.println("\t Gst: "+Gsm_Info_Bean._storeTin);
		/**
		 * Menu for Login as Owner of Employee and  and logic to redirect menu for customer product division to main menu.
		 */
		System.out.println("\n 1.Owner \n 2.Employee \n 3.Exit");
		String _login_as=read();
		if(_login_as.equalsIgnoreCase("1")) 
		{
			if(!addProduct());
			{
				System.out.println("\n \n");
				init();
			}
		}
		else if (_login_as.equalsIgnoreCase("2"))
		{
			if(!sellProducts())
			{
				System.out.println("\n \t \t Total: "+total_service()+"\n \n");
				tempdelete();
			}
		}
		else if (_login_as.equalsIgnoreCase("3")) 
		{
			System.exit(0);
		}
		
		return false;
		
	}

	@Override
	public String read() {
		// TODO Auto-generated method stub
		try {
			return new BufferedReader(new InputStreamReader(System.in)).readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void writeHelper(Storage st) {
		// TODO Auto-generated method stub
		File file=new File(st.info());
		FileWriter writer=null;
		try {
			writer=new FileWriter(file,true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			writer.write(st.write()+"\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void listhelper(Gsm_System_Bean_Warehouse gsw)
	{
		File file=new File(gsw.info2());
		FileWriter writer=null;
		try {
			writer=new FileWriter(file,true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			writer.write(gsw.write2()+"\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void totalhelper(Total_Helper th)
	{
		File file=new File(th.tempinfo());
		FileWriter writer=null;
		try {
			writer=new FileWriter(file,true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			writer.write(th.tempwrite()+"\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	/**
	 * @author rahul
	 */
	//--------------------------------------------------------owner's perspective---------------------------------------------//
	public boolean addProduct()
	{
		Gsm_System_Bean_Warehouse _warehouse=new Gsm_System_Bean_Warehouse();
		System.out.println("\n Enter the product Name/Brand");
		_warehouse.setProduct_name(read());
		System.out.println("\n Enter the product type: ");
		_warehouse.setProduct_Type(read());
		System.out.println("\n Enter product price: ");
		_warehouse.setProduct_price(Integer.parseInt(read()));
		System.out.println("\n Enter the no of units arrived: ");
		_warehouse.setNos(Integer.parseInt(read()));
		writeHelper(_warehouse);
		listhelper(_warehouse);
		return false;
		
	}
	//-------------------------------------------------------Employee's perspective-------------------------------------------//
	
	public boolean sellProducts()
	{
		Gsm_System_Bean_Store _store=new Gsm_System_Bean_Store();
		/**
		 * @author rahul
		 * Buffers Declaralion
		 */
		ArrayList<Integer> _list=new ArrayList<Integer>();
		ArrayList<String> _productcart=new ArrayList<String>();
		ArrayList<Integer> _quatity=new ArrayList<Integer>();
		ArrayList<Integer> product_initial_price=new ArrayList<Integer>();
		/**
		 * @author rahul
		 * Inputs taking with validation(s).
		 */
		fileread();
		try {
		System.out.println("\n Enter the product name");
			_store.set_warehouse(getWareHouse(read()));
			System.out.println("\n Enter the quatity you want to purchase");
			_store.setQuantity(Integer.parseInt(read()));
				quatityChecker(_store.getQuantity());
			writeHelper(_store);
			totalhelper(_store);
			/**
			 * Buffers to store the existing values and to print those values in the cart.
			 * @author rahul
			 */
			_list.add(_store.getTotal());
			_productcart.add(_store.get_warehouse().getProduct_name());
			_quatity.add(_store.getQuantity());
			product_initial_price.add(_store.get_warehouse().getProduct_price());
			/**
			 * @author rahul
			 * Option to continue shopping and checkout the cart.
			 */
			System.out.println("\n Do you want to continue shopping(y/n):");
			String ch=read();
			if(ch.equalsIgnoreCase("y")) {
				sellProducts();
				
				
			}
			if(ch.equalsIgnoreCase("n"))
			{
				System.out.println("\n Products in the cart are: ");
				System.out.println("\t Products \t price \t quatity\t total");
			}
			for(int i=0;i<_list.size();i++)
			{
				System.out.println("\t "+_productcart.get(i)+"\t "+product_initial_price.get(i)+"\t "+_quatity.get(i)+"\t "+_list.get(i));
			}
		}
		 catch (Productnotfound e1) {
			// TODO Auto-generated catch block
			System.out.println("\n "+e1.getMessage()+"\n");
			System.out.println("Continue shopping with the available products below");
			sellProducts();
		}
		catch (Outofstock e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage()+"\n");
			System.out.println("Product quantity is out of stock as per your quantity input");
			sellProducts();
		
		}
		return false;
		
	}
	//-----------------------------------------List display---------------------------------------------//
	public String fileread() {
		// TODO Auto-generated method stub
		String path=Gsm_System_Bean_Warehouse._path;
		String _file=Gsm_System_Bean_Warehouse.Products;
		String display;
		BufferedReader bo=null;
		try {
			bo=new BufferedReader(new FileReader(path+File.separator+_file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			System.out.println(" Product Type Price ");
			System.out.println(" ");
			while((display=bo.readLine())!=null)
			{
				System.out.println(":- "+display);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	//------------------------------------------------------ File Traversers --------------------------------------------------//
	public Gsm_System_Bean_Warehouse getWareHouse(String setInput) throws Productnotfound // for products existence
	{
		String path=Gsm_System_Bean_Warehouse._path;
		String file=Gsm_System_Bean_Warehouse.WareHouse;
		BufferedReader reader=null;
		try {
			reader=new BufferedReader(new FileReader(path+File.separator+file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String _checker;
		try {
			while((_checker=reader.readLine())!=null)
			{
				String data[]=_checker.split(",");
				if(data[2].equalsIgnoreCase(setInput)==true)
				{
					Gsm_System_Bean_Warehouse _ware=new Gsm_System_Bean_Warehouse();
					_ware.setWarehouse_id(data[0]);
					_ware.setProduct_id(data[1]);
					_ware.setProduct_name(data[2]);
					_ware.setProduct_price(Integer.parseInt(data[3]));
					_ware.setProduct_Type(data[4]);
					_ware.setNos(Integer.parseInt(data[5]));
					return _ware;
				}
			}
			throw new Productnotfound("product is not in our inventory");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
	
	/**
	 * @author rahul
	 * for quantity check
	 * @throws Outofstock 
	 */
	public Gsm_System_Bean_Warehouse quatityChecker(int i) throws Outofstock{
		String path=Gsm_System_Bean_Warehouse._path;
		String file=Gsm_System_Bean_Warehouse.WareHouse;
		BufferedReader buffer=null;
		try {
			buffer=new BufferedReader(new FileReader(path+File.separator+file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String _qtchecker;
		try {
			while ((_qtchecker=buffer.readLine())!=null) {
				String data[]=_qtchecker.split(",");
				if(Integer.valueOf(data[5])>=i)
				{
					Gsm_System_Bean_Warehouse _ware=new Gsm_System_Bean_Warehouse();
					_ware.setWarehouse_id(data[0]);
					_ware.setProduct_id(data[1]);
					_ware.setProduct_name(data[2]);
					_ware.setProduct_price(Integer.parseInt(data[3]));
					_ware.setProduct_Type(data[4]);
					_ware.setNos(Integer.parseInt(data[5]));
					return _ware;
				}
			}
			throw new Outofstock("Out of Stock");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		return null;
	}
	//--------------------------------- Total counter---------------------------------------------------//
	public int total_service()
	{
		int sum=0;
		String _file=Gsm_System_Bean_Store.Temp;
			File file=new File(_file);
			try {
				Scanner sc=new Scanner(file);
				 sum=0;
				while (sc.hasNext()) {
					sum+=sc.nextInt();
					
				}
				sc.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		return sum;
		
	}
	//--------------------------------------------------------- Temp Delete---------------------------------------------//
	/**
	 * @author rahul
	 * this method will help you to delete the TempBill.csv after the checkout of the customer.
	 */
	public void tempdelete()
	{
		String path=Gsm_System_Bean_Store.path;
		String _file=Gsm_System_Bean_Store.Temp;
		File file=new File(path+File.separator+_file);
		if(file.delete())
		{
			init();
		}
		else
		{
			init();
		}
	}
	
	
	
}
