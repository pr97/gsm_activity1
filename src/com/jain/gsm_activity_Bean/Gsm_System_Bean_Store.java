package com.jain.gsm_activity_Bean;

import java.io.File;
import java.util.Date;

public class Gsm_System_Bean_Store implements Storage,Total_Helper {
	public static final String path=".";
	public static final String store="Bill.csv";
	public static final String Temp="BillTemp.csv";
	private Gsm_System_Bean_Warehouse _warehouse;
    private long bill_no;
	private Date _date;
	private int quantity;
	int total;
	

	public Gsm_System_Bean_Store() {
		bill_no=System.currentTimeMillis()*8;
		_date=new Date();
	}

	@Override
	public String info() {
		// TODO Auto-generated method stub
		return path+File.separator+store;
	}

	@Override
	public String write() {
		// TODO Auto-generated method stub
		return getBill_no()+","+get_date()+","+get_warehouse().getProduct_name()+","+get_warehouse().getProduct_id()+","+get_warehouse().getProduct_price()+","+getQuantity()+","+getTotal()+","+get_warehouse().getWarehouse_id();
	}

	public long getBill_no() {
		return bill_no;
	}

	public void setBill_no(String bill_no) {
		this.bill_no = Long.parseLong(bill_no);
	}

	public Gsm_System_Bean_Warehouse get_warehouse() {
		return _warehouse;
	}

	public void set_warehouse(Gsm_System_Bean_Warehouse _warehouse) {
		this._warehouse = _warehouse;
	}

	public Date get_date() {
		return _date;
	}

	public void set_date(Date _date) {
		this._date = _date;
	}
	
	public int getTotal()
	{
		return get_warehouse().getProduct_price()*getQuantity();
	}
	public void setTotal(int total)
	{
		this.total=total;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String info2() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String write2() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String tempinfo() {
		// TODO Auto-generated method stub
		return path+File.separator+Temp;
	}

	@Override
	public String tempwrite() {
		// TODO Auto-generated method stub
		return getTotal()+" ";
	}
	
	

}
