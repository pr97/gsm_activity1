package com.jain.gsm_activity_Bean;

import java.io.File;

public class Gsm_System_Bean_Warehouse implements Storage 
{
	public static final String _path=".";
	public static final String Products="products.csv";
	public static final String WareHouse="Warehouse.csv";
	private long warehouse_id;
	long product_id;
	private String product_name;
	String product_Type;
	int product_price; 
	private int nos;
	
	public Gsm_System_Bean_Warehouse() {
		// TODO Auto-generated constructor stub
		warehouse_id=System.currentTimeMillis()*9;
		product_id=System.currentTimeMillis()*10;
	}
	
	@Override
	public String write() {
		// TODO Auto-generated method stub
		return getWarehouse_id()+","+getProduct_id()+","+getProduct_name()+","+getProduct_price()+","+getProduct_Type()+","+getNos();
	}
	@Override
	public String info() {
		// TODO Auto-generated method stub
		return _path+File.separator+WareHouse;
	}
	public long getProduct_id() {
		return product_id;
	}
	public void setProduct_id(String product_id) {
		this.product_id = Long.parseLong(product_id);
	}
	public String getProduct_Type() {
		return product_Type;
	}
	public void setProduct_Type(String product_Type) {
		this.product_Type = product_Type;
	}
	public int getProduct_price() {
		return product_price;
	}
	public void setProduct_price(int product_price) {
		this.product_price = product_price;
	}
	public long getWarehouse_id() {
		return warehouse_id;
	}
	public void setWarehouse_id(String warehouse_id) {
		this.warehouse_id = Long.parseLong(warehouse_id);
	}


	public int getNos() {
		return nos;
	}


	public void setNos(int nos) {
		this.nos = nos;
	}


	public String getProduct_name() {
		return product_name;
	}


	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	@Override
	public String info2() {
		// TODO Auto-generated method stub
		return _path+File.separator+Products;
	}

	@Override
	public String write2() {
		// TODO Auto-generated method stub
		return getProduct_name()+","+getProduct_Type()+","+getProduct_price();
	}

}
