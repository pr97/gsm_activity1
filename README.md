**Getting Started**

`git clone https://gitlab.com/pr97/gsm_activity1.git`

*Prefered IDE*

*  Eclipse

**Command to Run the .jar file**

`java -jar GSM.jar`

**To Know more about the project refer the below documentation**

[Documentation](https://docs.google.com/document/d/1VzRQzGg9_1dXaxSG9KspjlcpZra10XAcuSjKmArfEvM/edit?usp=sharing)

Any quries contact me:
> prithvish.rahul97@outlook.com 